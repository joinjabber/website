#!/usr/bin/env bash

# Adapted from https://github.com/darktable-org/dtdocs/blob/master/tools/generate-translations.sh
# Requires po4a version 0.58 or higher.

# To allow po4a to manage a language, add it to the "enabled-languages" file.
# Otherwise po4a will ignore it even though PO files for this language exist.

set -euo pipefail

# Is po4a new enough?
function version_gt() { test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1"; }

PARAM=${1:-}

# Create a temporary po4a config file.
function generate_po() {
  local languages=${1:-}
  local section=${2:-}
  local depth=${3:-99}

  po4a_conf=$(mktemp)
  # po4a_conf=po/po_config
  echo "[po4a_langs] $languages" >"$po4a_conf"
  echo "[po4a_paths] po/${section}.pot \$lang:po/${section}.\$lang.po" >>"$po4a_conf"
  cat >>"$po4a_conf" <<EOF

[options] opt:--addendum-charset=UTF-8 opt:--localized-charset=UTF-8 opt:--master-charset=UTF-8 opt:--porefs=file opt:--master-language=en_US opt:--wrap-po=newlines opt:--copyright-holder=JoinJabber opt:--package-name=JoinJabber-Website

[po4a_alias:markdown] text opt:--option markdown opt:--option yfm_keys=title opt:--addendum-charset=UTF-8 opt:--localized-charset=UTF-8 opt:--master-charset=UTF-8 opt:wrap-po=newlines

EOF
  # for f in $section/*.en.md; do
  find content -maxdepth "$depth" -type f -name '*.en.md' | while read -r f; do
    echo "[type: markdown] $f \$lang:$(dirname $f)/$(basename $f .en.md).\$lang.md" >>"$po4a_conf"
  done
  po4a "$PARAM" --verbose "$po4a_conf" &
}

function main() {
  # Is po4a installed?
  if ! which po4a; then
    echo "ERROR: Install po4a from your package manager."
    exit 1
  fi

  required_version='0.58'
  current_version=$(po4a --version | sed -En 's,po4a version ([0-9][0-9.]+[0-9]).*,\1,pi')
  if version_gt "$required_version" "$current_version"; then
    echo "ERROR: po4a v0.58 or higher required."
    exit 1
  fi

  # Is the script argument correct
  if [ "$PARAM" == "--no-update" ]; then
    echo "Generating the translated files."
  elif [ "$PARAM" == "--no-translations" ]; then
    echo "Generating the POT and PO files."
  elif [ "$PARAM" == "--rm-translations" ]; then
    echo "Removing the translated files."
  else
    echo "The argument to this script must be one of '--no-update', '--no-translations', or '--rm-translations'."
    exit 1
  fi

  local enabled_languages=$(cat enabled-languages 2>/dev/null || true)
  local languages=

  # Ensure po files exist for enabled languages
  for lang in $enabled_languages; do
    echo "Lang: $lang"
    if [[ $(find po -name "*.$lang.po" 2>/dev/null) ]]; then
      languages="$languages $lang"
    fi
  done

  echo "Languages: $languages"

  generate_po "$languages" "content"
  wait

  # no need to keep these around
  if test -f po/*.en.po; then
    rm -f po/*.en.po
  fi
}

main
