(use-modules
                                        ; gcc-toolchain this is needed for hugo
 (gnu packages commencement)
                                        ; for git
 (gnu packages version-control)
                                        ; for nss-certs
 (gnu packages certs)
 (gnu packages bash)
 (gnu packages gettext)
                                        ; po4a package
 (bechamel packages gettext)
                                        ; hugo extended package
 (bechamel-staging packages hugo))


                                        ; the packages actually needed in the container.
                                        ; git because there is an autoupdate mechanism
(packages->manifest (list git
                                        ; so that the git repos can be cloned with https
			              nss-certs
                                        ; the package we defined above for translations
                          po4a
                                        ; the hugo package we defined above
			              hugo_extended
                                        ; needed by hugo_extended specifically. plain hugo can work without this
			              gcc-toolchain
                                        ; all these are used in the Makefile or the script
                          gnu-make
                          sed
                          which
                          findutils
                          coreutils
                          bash))
