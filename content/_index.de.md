---
title: "JoinJabber"
---

{{< block "grid-2" >}}
{{< column >}}

# Willkommen bei der JoinJabber Gemeinschaft

_Ein inklusiver Ort im Jabbernetzwerk_

{{< tip "warning" >}}
Mit Jabber kannst du [sicher](docs/faqs/user/#faq-users-encryption) chatten oder deine Freunde und Familie anrufen. Oder du kannst öffentliche Gruppenchats mit Leuten die ähnliche Interessen haben besuchen, ohne dabei deine persönlichen Daten offenlegen zu müssen.
{{< /tip >}}

{{< tip >}}
Jabber, [auch bekannt als **XMPP**](docs/faqs/user/#faq-users-xmppjabber), ist ein _offener Standard_ für Onlinekommunikation. Dies bedeutet, dass das Jabbernetzwerk uns allen gehöhrt und nicht nur einer einzelnen Organisation.

Jabber ist [föderiert](docs/faqs/user/#faq-users-federation), ähnlich wie E-Mail oder Mastodon. Viele Server sind miteinander verbunden und erschaffen so zusammen das Jabbernetzwerk.
{{< /tip >}}

{{< tip "warning" >}}
Auf dieser Webseite helfen wir dir in zwei einfachen Schritten an Jabber teilzunehmen.

Ausserdem laden wir dich ein Teil [unseres Projektes](about/goals/) zu werden und die Möglichkeiten der Jabberplatform zu erweitern.
{{< /tip >}}

{{< button "docs/" "Los geht's!" >}}{{< button "https://chat.joinjabber.org/" "Chatte mit uns" >}}
{{< /column >}}

{{< column >}}
![](/images/undraw_texting.svg)
{{< /column >}}
{{< /block >}}
