---
title: "Collectieve doelstellingen"
weight: 2
---

Toen we ons collectief oprichtten, hebben we onszelf een aantal doelen gesteld. Op deze pagina vind je de bijgewerkte lijst van onze doelen. Deze doelen zijn tijdens onze bijeenkomsten in het Engels besproken. Bijdragen zijn welkom.

- JoinJabber is een gemeenschap gewijd aan de belangen van eindgebruikers van het federatieve netwerk Jabber
- JoinJabber bevordert culturele diversiteit en internationale samenwerking: we willen mensen in staat stellen te communiceren zonder enige vorm van onderdrukking of barrières; elke taal is welkom in de gemeenschap, hoewel elke taal zijn eigen kanalen heeft (forums, chats...).
- Communicatie en besluitvorming binnen het JoinJabber-collectief vindt plaats in het Engels en we moedigen sprekers van andere talen aan om hun zorgen te uiten via vertalingen en/of afgevaardigden van hun keuze. De geplande agenda voor een vergadering wordt gepubliceerd en op voorhand vertaald op een "best effort"-basis, net als de notulen van de vergadering.
- JoinJabber is een vrije vereniging, die empowerment van individuen en gemeenschappen promoot; elke persoon is vrij om wensen en zorgen te uiten tijdens onze vergaderingen en we hebben geen formeel lidmaatschap
- JoinJabber is tegen giftig en onderdrukkend gedrag zoals stalking, intimidatie en spamming; we verbannen mensen die onze gebruikers en onze gemeenschap schade berokkenen
- JoinJabber is niet betrokken bij enige vorm van commerciële activiteit: we kunnen donaties inzamelen voor de JoinJabber-infrastructuur, adverteren voor bestaande inzamelingsacties voor oplossingen die we aanbevelen (clients, servers en serverbeheerders), of deelnemen aan inzamelingsacties voor nieuwe features die interessant zijn voor eindgebruikers
- JoinJabber bevordert gedecentraliseerd bestuur: we hebben geen autoriteit over welk project dan ook, hoewel we ervoor kunnen kiezen bepaalde projecten aan te bevelen of niet, afhankelijk van verder vastgestelde criteria
- JoinJabber bevordert samenwerking tussen projecten en wil de adoptie van moderne standaarden gericht op de behoeften van eindgebruikers aanmoedigen
- JoinJabber bevordert gedecentraliseerde internetinfrastructuur: we willen geen grote centrale server worden, maar bevorderen bestaande gebruikersvriendelijke servers en het zelf hosten van nieuwe servers
- JoinJabber levert diensten en infrastructuren voor collectieve projecten rond het Jabber-ecosysteem, maar levert geen diensten aan eindgebruikers (zoals Jabber-accounts)
- JoinJabber biedt ondersteuningskanalen (instant chat en langdurige discussies) om gebruikers van Jabber-clients/servers te helpen; we willen geen bestaande ondersteuningskanalen voor specifieke projecten vervangen, maar wel helpen hun last te verlichten bij eenvoudige zaken; de ondersteuning wordt verleend door vrijwilligers op een "best effort"-basis
- JoinJabber promoot privacy als een fundamenteel mensenrecht en als een pijler van de macht en autonomie van het volk; we nemen deel aan de evaluatie van de privacy van het Jabber-ecosysteem en doen aanbevelingen voor uitvoerders, voor serverbeheerders en voor eindgebruikers; het gebruik van een nickname wordt aanbevolen (maar is niet verplicht) om deel te nemen aan het JoinJabber-collectief
- JoinJabber bestudeert gebruikerservaringen in het gehele Jabber-ecosysteem en verzamelt feedback van eindgebruikers; deze feedback, verzameld uit getuigenissen en praktijkstudies (bijvoorbeeld tijdens installatieparty's), kan worden gebruikt om aanbevelingen te doen aan uitvoerders en serverbeheerders
- JoinJabber bevordert de interoperabiliteit tussen Jabber en andere vrije, gedecentraliseerde netwerken; we kunnen alleen op tegen de internetgiganten als we in staat zijn te communiceren tussen vrije oplossingen (zoals IRC, Matrix, Fediverse...)
- JoinJabber bevordert de toegankelijkheid van alle diensten, ongeacht fysiologie (handicaps) en beperkingen in middelen (trage internettoegang, low-end hardware, of gebrek aan geld).
- JoinJabber moedigt reproduceerbare builds aan voor meer betrouwbare en vertrouwenwekkende software. JoinJabber kan softwarerepositories aanbieden om snellere, veilige updates voor bestaande client-/serversoftware over besturingssystemen aan te moedigen.
