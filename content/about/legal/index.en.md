---
title: "Legal"
description: "Credits etc."
weight: 3
---

## Terms of service

By using our services you agree to adhere to our [Code of Conduct](about/community/codeofconduct).

## Privacy Policy

Our services are hosted in the EU and all relevant local privacy laws like the GDPR apply. We do not set any non-functional cookies or store any personal data.

## Website attribution

Website source can be found [here](https://codeberg.org/joinjabber/website).

It is based on the [Compose theme](https://github.com/onweru/compose), by onweru.

To build the site, [Hugo](https://gohugo.io/) is used.

[JoinJabber logo and favicon designs](https://codeberg.org/joinjabber/collective) are contributed by Line with small modifications by Guillaume.

The font used in the logo is ["Hey October"](https://www.dafont.com/hey-october.font) by Syafrizal a.k.a. Khurasan.

undraw_*.svg vector images are open-source illustrations from [unDraw](https://undraw.co/).

Service and app logos are copyrighted by their respective owners. 

## XMPP software used

We use [Prosody](https://prosody.im) as our XMPP server to host the JoinJabber chat-rooms.

We use [XMPP-web](https://github.com/nioc/xmpp-web) as the web-frontend for unregistered access and channel listing.
