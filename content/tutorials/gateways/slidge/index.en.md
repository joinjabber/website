---
title: "Slidge Gateways"
---

[Slidge](https://slidge.im/) is a library for making [puppeteering gateways](/docs/faqs/gateways/#terminology). This means that contrary to other options an account on the remote network is still required. The Slidge gateway then logs into this account and lets you remote control it via XMPP.

Currently Slidge based gateways are available for:

- [Discord](https://slidge.im/slidcord/) ([source-code](https://git.sr.ht/~nicoco/slidcord))
- [Facebook Messenger](https://slidge.im/messlidger/) ([source-code](https://git.sr.ht/~nicoco/messlidger))
- [Mattermost](https://slidge.im/matteridge/) ([source-code](https://git.sr.ht/~nicoco/matteridge))
- [Matrix](https://slidge.im/matridge/) ([source-code](https://git.sr.ht/~nicoco/matridge))
- [Signal](https://slidge.im/slidgnal/) ([source-code](https://git.sr.ht/~nicoco/slidgnal))
- [Skype](https://slidge.im/skidge/) ([source-code](https://git.sr.ht/~nicoco/skidge))
- [Steam Chat](https://slidge.im/sleamdge/) ([source-code](https://git.sr.ht/~nicoco/sleamdge))
- [Telegram](https://slidge.im/slidgram/) ([source-code](https://git.sr.ht/~nicoco/slidgram))
- [WhatsApp](https://slidge.im/slidge-whatsapp/) ([source-code](https://git.sr.ht/~nicoco/slidge-whatsapp))

### Known public instances

There are currently no known public instances of Slidge. Due to the need to store the legacy-network's user credentials on the server it is unlikely that true public instances will be set up. However, your XMPP server admin (which might be yourself) could offer it as a trusted service.

### Write your own Slidge gateway

Slidge is designed to be a framework for gateways and can be [easily extended](https://slidge.im/core/dev/tutorial.html) if a library (ideally in python), a CLI client, or a web API for the legacy network exists.
