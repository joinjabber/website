---
title: "Matrix Gateway"
---

Here, you'll learn how to connect to a [Matrix](https://matrix.org) room, with a gateway called called [Bifrost](https://github.com/matrix-org/matrix-bifrost). For other Matrix to XMPP bridges please see the [official list](https://matrix.org/ecosystem/bridges/xmpp/) on their website.

For a puppeteering gateway (to use with an existing Matrix account) please refer to [Slidge](/tutorials/gateways/slidge/).

## Known public instances

- [aria-net.org](https://archon.im/arianet/matrix-homeserver/services/) (strongly recommended)
- [matrix.org](https://github.com/matrix-org/matrix-bifrost) (lacking functionality)

## Connection syntax

- Direct chats: `USERNAME_DOMAIN@aria-net.org`
- Public chats: `#ALIAS#DOMAIN@aria-net.org`

## Bridging rooms

It also supports plumbing of existing Matrix rooms to XMPP rooms, Matrix room administrators can perform plumbing by doing the following:

- Invite `@_bifrost_bot:aria-net.org` into the room
- type `!bifrost bridge xmpp-js component.domain.tld roomname`
- To eventually remove the plumbing you can just type `!bifrost leave`

## Connecting from Matrix

Matrix users can also connect to Jabber/XMPP through the same gateway. Connection syntax:

- Private chats: `@_bifrost_USER=40DOMAIN:aria-net.org`
- MUCs: `#_bifrost_MUCNAME_MUCDOMAIN:aria-net.org`



