---
title: "Matrix Gateway"
---

Hier zul je in de toekomst zien hoe je verbinding maakt met een [Matrix](https://matrix.org) room, gebruikmakend van een gateway genaamd [Bifrost](https://matrix.org/docs/projects/bridge/matrix-bifrost).

Voor een "puppeteering gateway" (waarbij een bestaande Matrix-account door de gateway wordt gebruikt), kijk bij [Slidge](../slidge/).

## Bekende openbare instanties

- [aria-net.org](https://archon.im/arianet/matrix-homeserver/services/) (sterk aanbevolen)
- [matrix.org](https://matrix.org/docs/projects/bridge/matrix-bifrost) (ontbreekt aan functionaliteit)

## Verbindingssyntax

- Directe chats: `GEBRUIKERSNAAM_DOMEIN@aria-net.org`
- Openbare chats: `#ALIAS#DOMEIN@aria-net.org`

## Rooms doorverbinden

Bifrost ondersteunt ook het doorverbinden van bestaande Matrix-rooms naar XMPP-groepschats. Beheerders van Matrix-room kunnen dit als volgt instellen:

- Nodig `@_bifrost_bot:aria-net.org` uit in de room
- Typ `!bifrost bridge xmpp-js component.domain.tld roomname`
- Om de verbinding weer op te heffen, typ je `!bifrost leave`.

## Verbinden vanuit Matrix

Gebruikers van Matrix kunnen via dezelfde gateway ook verbinding maken met Jabber/XMPP. Syntax:

- Privéchats: `@_bifrost_USER=40DOMEIN:aria-net.org`
- Groepschats: `#_bifrost_GROEPNAAM_GROEPDOMEIN:aria-net.org`.
