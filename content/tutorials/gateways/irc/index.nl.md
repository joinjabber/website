---
title: "IRC Gateway"
---

[IRC](https://nl.wikipedia.org/wiki/Internet_Relay_Chat) is een chatprotocol.
Het is gericht op groepscommunicatie, maar staat ook één-op-één communicatie toe.
In tegenstelling tot Jabber kunnen IRC-gebruikers van een netwerk/server alleen communiceren met kanalen en nicks van dezelfde server.
In IRC zijn kanalen groepschats (zoals MUC in Jabber) en nicks zijn gebruikers-identifiers (zoals JID's in Jabber).
Om meer te leren over IRC, zie [IRC-Help](https://www.irchelp.org/).

Hieronder volgt hoe je verbinding maakt met IRC vanuit je Jabber-account, via een IRC-transport voor Jabber, genaamd [Biboumi](https://biboumi.louiz.org/).

**Tip!** Iedereen kan een transport op zijn server hosten: als je Biboumi op je server wilt opzetten, raadpleeg dan [Self-Host](https://doc.biboumi.louiz.org/).

## Selectie {#selection}

### Een IRC-netwerk vinden

Er zijn veel IRC-netwerken [beschikbaar](https://www.irchelp.org/networks/). Enkele van de populaire IRC netwerken zijn:
- `irc.libera.chat` door [LiberaChat](https://libera.chat/)
- `irc.oftc.net` door [OFTC](https://oftc.net/)
- `irc.chalec.org` door [Chalec](https://www.chalec.org/services/xmpp.html)
- `irc.chapril.org` door [Chapril](https://www.chapril.org/xmpp)

**Tip!** Je kunt je nick registreren op een netwerk (volg de handleiding op de site van het betreffende netwerk), omdat sommige kanalen dit vereisen.

### Een IRC-transport vinden

Sommige instanties zijn privé, terwijl andere openbaar zijn.
Privé-instanties zijn alleen toegankelijk vanaf accounts op dezelfde server, terwijl openbare instances toegankelijk zijn vanaf elk account.

**Waarschuwing!** Wees je ervan bewust dat sommige IRC-netwerken openbare transporten kunnen verbieden om spam te bestrijden.

#### Openbaar

- `irc.jabber.hot-chilli.net` door [Hot-Chilli](https://jabber.hot-chilli.net)
- `irc.hmm.st` door [Hmm](https://hmm.st)
- `irc.jabberfr.org` door [JabberFR](https://jabberfr.org/)
- `irc.cheogram.com` door [Sopranica](https://soprani.ca/)
- `irc.chalec.org` door [Chalec](https://www.chalec.org/services/xmpp.html)
- `irc.chapril.org` door [Chapril](https://www.chapril.org/xmpp)

**Pas op!** Hot-Chilli vereist dat je vooraf je JID [registreert](https://jabber.hot-chilli.net/forms/preregister/), voordat je het transport gebruikt.

#### Privaat

- `irc.disroot.org` door [Disroot](https://disroot.org)

## Verbinding {#connection}

Je moet het transport vertellen op welk netwerk je wilt verbinden, en op welk kanaal of nick je wilt chatten.
We gebruiken het `%`-teken om deze twee stukjes informatie af te bakenen.

**Let op!** Kanaalnamen in IRC worden altijd voorafgegaan door `#`.

### Kanalen

Om verbinding te maken met IRC-kanalen is de syntaxis `#kanaal%irc.netwerk.tld@irc.transport.tld`.
Hier staat '#kanaal' voor het IRC-kanaal (bv. #fsf), 'irc.netwerk.tld' voor het IRC-netwerk (bv. irc.libera.chat), en 'irc.transport.tld' voor IRC-transport (bv. irc.disroot.org).

### Nicks

Om te verbinden met IRC-kanalen, is de syntax `nick%irc.netwerk.tld@irc.transport.tld`.
Hier stelt 'nick' de IRC nick voor (bv. doe), 'irc.network.tld' stelt het IRC-netwerk voor (bijv. irc.oftc.net), en 'irc.transport.tld' stelt IRC-transport voor (bijv. irc.snopyta.org).

## Authenticatie {#authentication}

Na verbinding zul je bericht ontvangen van het IRC-netwerk (als irc.netwerk.tld@irc.transport.tld) om in te loggen als het netwerk/kanaal dit vereist.
Je moet antwoorden aan het IRC netwerk in de vorm `NickServ IDENTIFY MijnLangWachtwoord` voor authenticatie.
Als de authenticatie succesvol is, zal de server je dat vertellen.

**Let op!** In tegenstelling tot Jabber, waar de authenticatie in chatrooms wordt uitgevoerd door je Jabber-server, wordt de authenticatie in IRC-ecosystemen apart behandeld door elk IRC-netwerk. Dit betekent dat als je kanalen binnengaat op meerdere netwerken, je je apart moet authenticeren op elk van die netwerken.

## Instellingen {#settings}

Er zijn drie verschillende niveaus van instellingen die je kunt configureren wanneer je Biboumi gebruikt, elk met een eigen configuratieadres:

- IRC-Transportniveau (per transport): `irc.transport.tld` (bijv. irc.jabber.hot-chilli.org)
- IRC-Netwerkniveau (per server): `irc.network.tld@irc.transport.tld` (bijv. irc.gimp.org@irc.jabber.hot-chilli.org)
- IRC-Kanaalniveau (per kanaal): `#channel%irc.network.tld@irc.transport.tld` (bijv. #gnome%irc.gimp.org@irc.jabber.hot-chilli.org)

**Let op!** Elk instellingenniveau zal de algemene instellingen overschrijven in het vorige niveau.

De instellingen worden geconfigureerd door wat we noemen ad-hoc commando's, ondersteund door veel clients.
In Gajim kun je rechts-klikken op het element dat je wilt configureren (bijv. IRC-transport, IRC-netwerk, of IRC-kanaal), om vervolgens te kiezen voor `Execute Command`.
Dit zal het corresponderende instellingenpaneel openen.

### Automatische authenticatie {#auto-authentication}

Het is mogelijk om automatisch te authenticeren op een IRC-netwerk.
Je moet de authenticatie-instellingen (gebruikersnaam/wachtwoord) configureren op het IRC-Netwerkniveau door ad-hoc commando's te gebruiken zoals eerder uitgelegd.

### Zelf hosten

Biboumi kan ook eenvoudig zelf gehost worden. [Lees hier meer](https://doc.biboumi.louiz.org/install.html).
