---
title: "Nextcloud"
---

How to easily integrate a XMPP server with Nextcloud. This tutorial is a work in progress, please check back later.

### LDAP

[Nextcloud](https://nextcloud.com/) can be easily integrated via LDAP. Please refer to the [official documentation](https://docs.nextcloud.com/server/latest/admin_manual/configuration_user/user_auth_ldap.html) and the [LLDAP configuration example](https://github.com/lldap/lldap/blob/main/example_configs/nextcloud.md).

For the integration with XMPP please refer to our [LDAP tutorial](/tutorials/integration/ldap).

### XMPP-cloud-auth

The [JSXC project](https://www.jsxc.org/) provides more advanced integration via [their Nextcloud app](https://apps.nextcloud.com/apps/ojsxc) and the [XMPP-cloud-auth software](https://github.com/jsxc/xmpp-cloud-auth). Sadly this integration is not yet updated to be compatible with the latest version of Nextcloud.

### User_external app

The [User_external app](https://github.com/nextcloud/user_external#xmpp-prosody) for Nextcloud also allows authenticating directly with a Prosody MySQL or MariaDB database.
