---
title: "Mastodon"
---

How to easily integrate an XMPP server with [Mastodon](https://joinmastodon.org/).

### Database integration

The easiest way to link an [Ejabberd server](https://www.ejabberd.im/) to a Mastodon instance on the same server is to use an [external auth script](https://git.chinwag.org/chinwag/ejabberd-auth-mastodon) that directly interacts with the Mastodon Postgresql database. This method only works securely on the local network, but has been well tested on a medium sized Mastodon instance.

### Mastodon bot for Ejabberd

Another easy to use option is to run a bot on your Mastodon instance that can be interacted with to register accounts via the Ejabberd API. You can find the necessary [bot software here](https://codeberg.org/spla/xmppbot.py). This bot also has some other nice functionality to get details from Ejabberd, so it might be worth running even if another account integration method is used.

### Via LDAP

If you are setting up a new Mastodon instance, you might also consider managing the user accounts via a separate LDAP database as explained in the [official Mastodon documentation here](https://docs.joinmastodon.org/admin/config/#ldap). Please refer to our [dedicated LDAP tutorial](/tutorials/integration/ldap) for the XMPP server part.

### Prosody

You might be able to adapt the [Prosody Sharkey integration](/tutorials/integration/sharkey/#prosody-http-auth), or use the new Oauth2 passthrough feature explained below.

{{< tip "warning" >}}
Currently Experimental.
{{< /tip >}}

Integration of a Prosody server with Mastodon can be done through OAuth2. The new [mod_auth_oauth_external](https://modules.prosody.im/mod_auth_oauth_external.html) module is needed for that (and it requires a recent nightly Prosody build).

First a new application needs to be registered from the Mastodon side. That can be done in Mastodon → Settings → Development → New application. Redirect URI doesn't matter. Prosody needs the `read:accounts` scope, noting else.

Then these lines need to be added to the Prosody configuration:

```lua
authentication = "oauth_external"
oauth_external_token_endpoint = "https://mastodon.example/oauth/token"
oauth_external_validation_endpoint = "https://mastodon.example/api/v1/accounts/verify_credentials"
oauth_external_username_field = "username"
oauth_external_scope = "read:accounts"
oauth_external_client_id = "Get client_id from https://mastodon.example/settings/applications"
oauth_external_client_secret = "Get client_secret from https://mastodon.example/settings/applications"
```

Now users can login with this username: `email\40example.com@xmpp.example.org`, which is composed of the email address they are registered with on the Mastodon server (with the ```@``` escaped as ```\40```) and the domain of the XMPP server behind the second ```@```. Hopefully this complex login name can be simplified in the future.

