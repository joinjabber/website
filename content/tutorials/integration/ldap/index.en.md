---
title: "LDAP"
---

How to easily integrate an XMPP server via LDAP.

[LDAP](https://wikipedia.org/wiki/Lightweight_Directory_Access_Protocol) is a widely used standard for integrating authentication in a local network. There are multiple independent implementations such as OpenLDAP, 389ds and many others. Here we will use [LLDAP](https://github.com/lldap/lldap/) as the reference, as it is very easy to setup and maintain, contrary to some more complex options like OpenLDAP.

### Ejabberd

[Ejabberd](https://www.ejabberd.im/) offers great LDAP support out of the box. LLDAP provides an [Ejabberd configuration example](https://github.com/lldap/lldap/blob/main/example_configs/ejabberd.md) in their repository. For basic authentication it is sufficient to add the following ```host_config``` to the main ```ejabberd.yml``` configuration file:

```yaml
host_config:
  xmpp.example.org:
    auth_method: [ldap]
    ldap_servers:
      - 127.0.0.1 #IP or hostname of LLDAP server
    ldap_port: 3890
    ldap_uids:
      - uid
    ldap_rootdn: "uid=lldap_readonly,ou=people,dc=example,dc=org"
    ldap_password: "secret"
    ldap_base: "ou=people,dc=example,dc=org"
```

Additional configuration like sharing vCard details and contacts lists are possible. Please refer to the [official Ejabberd documentation](https://docs.ejabberd.im/admin/configuration/ldap/).

### Prosody

[Prosody](https://prosody.im/) also supports LDAP authentication out of the box [via the built in](https://prosody.im/doc/modules/mod_auth_ldap) ```mod_auth_ldap```, however this depends on OpenLDAP, which is not available in some Linux distributions like Fedora.

The configuration options should be similar to the example for Ejabberd above. Please check back later as this part of the tutorial is still a work in progress.

### Openfire

[Openfire](https://igniterealtime.org/projects/openfire/) supports LDAP out of the box too. A guide can be found [here](https://download.igniterealtime.org/openfire/docs/latest/documentation/ldap-guide.html).

For a hands-on tutorial please check back later as this part of the tutorial is a work in progress.
