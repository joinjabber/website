---
title: "Sharkey"
---

How to easily integrate a XMPP server with a [Sharkey](https://activitypub.software/TransFem-org/Sharkey) microblogging instance:

*Note: This likely also works with Misskey and other forks, but this is currently untested.*

### Ejabberd

We are still looking into the best options to directly link Ejabberd to Sharkey, but it would be probably possible to adapt the [external auth script for Mastodon](/tutorials/integration/mastodon) to connect to a Sharkey Postgres database instead. Please let us know if you get this to work.

### Prosody http auth

The easiest way to link a Prosody XMPP server to a Sharkey instance is via the [Mod_auth_http](https://modules.prosody.im/mod_auth_http) module and a [custom middleware](https://activitypub.software/piuvas/sharkauth) that translates these http requests into direct lookups in the Sharkey Postgres database. If you are running Sharkey in containers, please ensure the database can be reached from this middleware software and the exposed http endpoint of it can be reached by Prosody. 

*Please note that these http requests are unencrypted and should only be done on the same server or in a secure internal network.*
