---
title: "Windows"
---

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-windows) {#gajim-windows}

[Gajim](https://gajim.org/) is a fully featured Jabber/XMPP app that runs on Windows. Get it [from their website](https://gajim.org/download/) or here:

[<img alt="Get it from Microsoft" src="/images/apps/microsoft.svg" style="min-height:65px;height:100%">](https://apps.microsoft.com/store/detail/gajim/9PGGF6HD43F9)

## [<img src="/images/apps/movim.svg" style="max-height:30px;height:100%"> Movim](#movim-windows) {#movim-windows}

[Movim](https://movim.eu) is a web-client that works well in browsers like Firefox or Chromium. It can be installed as a [PWA app](https://en.wikipedia.org/wiki/Progressive_web_app) by visiting a [Movim instance](https://join.movim.eu) and selecting the "Add to home screen" option from the menu of your browser. Push notifications can be enabled in the Movim settings and will be delivered through the browser's web-push feature.
