---
title: "GNU/Linux"
---

## [<img src="/images/apps/dino.svg" style="max-height:30px;height:100%"> Dino](#dino-linux) {#dino-linux}

[Dino](https://dino.im/) is a new modern looking Jabber/XMPP app for GNU/Linux. It supports audio & video and small conference calls. Get it from your distribution's package manager or here:

[<img alt="Get it on Flathub" src="/images/apps/flathub.svg" style="max-height:65px;height:100%">](https://flathub.org/apps/im.dino.Dino)

The Flatpak version of Dino also runs well on mobile Linux distributions like [PostmarketOS](https://postmarketos.org/) or [Mobian](https://mobian-project.org/).

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-linux) {#gajim-linux}

[Gajim](https://gajim.org/) is a fully featured Jabber/XMPP desktop app that runs on GNU/Linux. It focusses on a power-user feature set. Get it from your distribution's package manager or here:

[<img alt="Get it on Flathub" src="/images/apps/flathub.svg" style="max-height:65px;height:100%">](https://flathub.org/apps/org.gajim.Gajim)

## [<img src="/images/apps/movim.svg" style="max-height:30px;height:100%"> Movim](#movim-linux) {#movim-linux}

[Movim](https://movim.eu) is a web-client that works well in the Firefox or Chromium browser. It can be installed as a [PWA app](https://en.wikipedia.org/wiki/Progressive_web_app) by visiting a [Movim instance](https://join.movim.eu) and selecting the "Add to home screen" option from the menu of your browser. Push notifications can be enabled in the Movim settings and will be delivered through the browser's web-push feature.
