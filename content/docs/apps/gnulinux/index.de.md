---
title: "GNU/Linux"
---

## [<img src="/images/apps/dino.svg" style="max-height:30px;height:100%"> Dino](#dino-linux) {#dino-linux}

[Dino](https://dino.im/) ist eine modern aussehende Jabber/XMPP Anwendung für GNU/Linux. Audio- und Videochat wird unterstützt. Sie ist im Paketmanager der meisten Distributionen zu finden oder auch hier:

[<img alt="Get it on Flathub" src="/images/apps/flathub.svg" style="max-height:65px;height:100%">](https://flathub.org/apps/im.dino.Dino)

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-linux) {#gajim-linux}

[Gajim](https://gajim.org/) ist eine Jabber/XMPP Anwendung mit großen Funktionsumfang die auf GNU/Linux läuft. Sie ist im Paketmanager der meisten Distributionen zu finden oder auch hier:

[<img alt="Get it on Flathub" src="/images/apps/flathub.svg" style="max-height:65px;height:100%">](https://flathub.org/apps/org.gajim.Gajim)

