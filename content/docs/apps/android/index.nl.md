---
title: "Android"
---

De beste manier om Android-apps te installeren is via de [F-Droid store](https://f-droid.org/), de open-source app store voor Android. Het kost minder dan een minuut om te installeren en is erg eenvoudig in gebruik, zoals te zien valt in [deze video](https://youtu.be/o-kqQQqb-Sw). Het is mogelijk dat er een waarschuwing over het installeren van .apk-bestanden verschijnt, maar F-Droid is uitermate veilig. Met een druk op een installatieknop hieronder zal de app automatisch in F-Droid worden opgezocht.
*Opmerking*: Op sommige Android-apparaten kunnen de gratis versies van F-Droid afgesloten worden wanneer zij op de achtergrond draaien. Meer informatie en mogelijke oplossingen zijn verzameld op [Don't kill my app!](https://dontkillmyapp.com).
## [<img src="/images/apps/conversations.svg" style="max-height:30px;height:100%"> Conversations](#conversations-android) {#conversations-android}

De meest bekende client op Android is [Conversations](https://conversations.im). Je kunt Conversations downloaden via [F-Droid](https://f-droid.org/en/packages/eu.siacs.conversations/). Als je de ontwikkeling van Conversations wilt steunen, kun je de app ook kopen via de Play Store van Google, of [doneren](https://conversations.im/#donate) aan het project.

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/eu.siacs.conversations/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=eu.siacs.conversations)

*Opmerking*: Als jouw apparaat de gratis versie van F-Droid afsluit wanneer het op de achtergrond draait, dan kan het gebruik van de betaalde versie in de Play Store helpen, omdat die versie meldingen via Google's push-server kan ontvangen en de app zo weer gestart wordt.

## [<img src="/images/apps/cheogram.svg" style="max-height:30px;height:100%"> Cheogram](#cheogram-android) {#cheogram-android}

[Cheogram](https://cheogram.com/) is een fork van Conversations met wat extra functionaliteit en een focus op [gateways](../../faqs/gateways). Download de app via [de website](https://cheogram.com) of:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/com.cheogram.android/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=com.cheogram.android.playstore)

*Opmerking*: Voor gebruik 
## [<img src="/images/apps/monocles.svg" style="max-height:30px;height:100%"> Monocles Chat](#monocles-android) {#monocles-android}

[Monocles Chat](https://monocles.de/more/) is nog een fork van Conversations met een focus op gebruiksvriendelijke verbeteringen aan de interface. Installeer het via F-Droid of doneer aan de ontwikkelaar door de app via de Play Store aan te schaffen:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/de.monocles.chat/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=eu.monocles.chat)

## [<img src="/images/apps/movim.svg" style="max-height:30px;height:100%"> Movim](#movim-android) {#movim-android}

[Movim](https://movim.eu) is een web-client die goed werkt in mobiele browsers, zoals Firefox of Chromium. Het is te installeren als een [Progressive Web App](https://en.wikipedia.org/wiki/Progressive_web_app) door eerst een [Movim-instantie](https://join.movim.eu) te openen en vervolgens in je browsermenu de optie "Toevoegen aan beginscreen" te kiezen. Pushmeldingen kunnen worden ingeschakeld in de instellingen van Movim: meldingen zullen via de web-pushfunctie van de browser worden afgeleverd.


