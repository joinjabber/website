---
title: "Persoonlijk"
description: "Servers voor persoonlijk gebruik"
weight: 3
---

Misschien ben je op zoek naar een non-profit provider met een duurzaam economisch model en die meer kans heeft om over 20 jaar nog in bedrijf te zijn? Of heb je liever een server die anonieme toegang biedt via het Tor-netwerk? Hier zijn enkele aanbevelingen:

## Chalec

[<img alt="Create an account" src="/images/servers/chalec.png" style="max-height:100px;height:100%">](https://www.chalec.org/services/xmpp.html)

[Chalec.org](https://www.chalec.org/services/xmpp.html) is een niet-commerciële provider uit Frankrijk. Aanmelden voor hun dienst kan rechtstreeks vanuit [je app]({{< ref "/docs/apps" >}}).

## XMPP.social

[<img alt="Create an account" src="/images/servers/hookipa.png" style="max-height:60px;height:100%">](https://hookipa.net/register/new)

XMPP.social is één van de domeinen die je gratis kunt registreren bij [Hookipa.net](https://hookipa.net/). Hun servers staan in Duitsland.

## Disroot

[<img alt="Create an account" src="/images/servers/disroot.svg" style="max-height:40px;height:100%">](https://user.disroot.org/pwm/public/newuser)

Disroot is een niet-commercieel project gevestigd in Nederland en opgericht in 2015. Het wordt gerund door een groep vrijwilligers en biedt verschillende online diensten aan. Lees er meer over [op hun website](https://disroot.org/en/about).

## XMPP.is

[<img alt="Create an account" src="/images/servers/xmppis.png" style="max-height:60px;height:100%">](https://xmpp.is/account/register/)

[XMPP.is](https://xmpp.is/) is een op privacy gerichte provider met servers in IJsland die [aanmeldingen en directe toegang via het Tor-netwerk](https://xmpp.is/2021/11/10/tor-hsv3-registrations-are-now-open/) aanbiedt.

**Meer servers?**

Dit is uiteraard geen volledige lijst! Als je een echt goede aanbeveling hebt, neem dan [contact met ons op](xmpp:chat@joinjabber.org?join). 

Als je beheerder bent van een van deze diensten en het er niet mee eens bent om hier vermeld te worden, neem dan ook [contact met ons op](xmpp:servers@joinjabber.org?join).
