---
title: "Servers"
description: "Recommended servers"
weight: 2
---

First you need to decide which of the following categories best describe what you are looking for.

### I want an easy all-inclusive start:

{{< button "all-in-one/" "All-In-One" >}}

_This category is for people new to XMPP that are looking for an option that combines everything in an easy package._

### I am interested in a service for my:

{{< button "collective/" "Collective" >}}

_This category is for groups and organisations that need multiple accounts and professional hosting._

### I would like to open a:

{{< button "personal/" "Personal account" >}}

_This category is for individuals looking for a reliable place to open an account._

### Why are there so many options to chose from?

The Jabber network is a large and decentralized effort run by many people and organizations. There is no central company calling all the shots and protocol development is organized collectively through the [XMPP Standards Foundation](https://xmpp.org/about/xmpp-standards-foundation/). This means that there are a lot of servers to choose from.

### Migrating accounts

You changed or mind or made a mistake when originally selecting a server? Not a big deal! While account portability is still a [work in progress](https://docs.modernxmpp.org/projects/portability/) for most of the Jabber network, there is a website that mostly automates it for you [here](https://migrate.modernxmpp.org/). As it currently requires your user-credentials to function, it would be probably good to change the passwords of both accounts after using this tool.

### Can't I just get an account with you?

Sorry, but we do not provide Jabber accounts ourselves. You can learn more about the reasons why [here](/about).
